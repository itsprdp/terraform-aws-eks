# mediaWiki instances on AWS EKS

Automation script to create and manage the AWK EKS, and RDS instances via `terraform` and deploy the `MediaWiki` application instances on the `kubernetes` cluster, expose the service via a `Network Load Balancer`.

# Usage
`$ ./setup help`
```
  Usage:
  setup [option] [args...]

      init                       Initialise the credentials directories.

      prepare                    Check if all the dependecies are met.

      config-credentials         Configure AWS credentials to bootstrap
                                 and manage AWS resources via terraform.

      terraform-plan  [args]     Runs `terraform plan` which creates an
                                 execution plan. For more options run
                                 `setup terraform help`

      terraform-apply [args]     Runs `terraform apply` which performs
                                 the changes required to reach the desired
                                 state of the configuration. For more options
                                 run `setup terraform help`

      terraform       [args]     Run `terraform` command with options.
                                 For more options run`setup terraform help`

      destroy         [args]     Destroy k8s deployments and services, then
                                 performs `terraform destroy`. For more
                                 options run`setup terraform help`

      update-kubeconf            Copy the new EKS cluster k8s config to
                                 ~/.kube/config so that we can run kubectl commands.
                                 (Note: if there's an existing config file then it
                                 will renamed to ~/.kube/old_config.terraform_<timestamp>)

      init-k8s-nodes             After bootstrapping the EKS cluster and EC2
                                 worker nodes. We need to join these nodes to
                                 the k8s cluster since AWS doesn't mangge it
                                 for us yet.

      deploy-app                 Deploy a new mediawiki service to the k8s
                                 cluster and create a NLB balancer.
                                 (by default: creates three instances. Modify
                                 ./kube-files/mediawiki-deployment.yaml to
                                 increase or decrease the number of instances.)

      help                       Display help options.
```

### Binary Dependencies
- [`aws`](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) CLI: To manage the AWS resources via API.
- [`terraform`](https://www.terraform.io/downloads.html): To manage the AWS cloud infrastructure.
- [`aws-iam-authenticator`](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html): To generate access token for `kubectl` communicate with the EKS cluster.

## Installation Steps

**Step 1)**
Initialise the credentials directories.
> `$ ./setup init`

**Step 2)**
Check if all the dependencies are installed on your machine.
> `$ ./setup prepare`

**Step 3)**
Configure the AWS credentials so that we can manage AWS resources via terraform. Follow the [steps here](./aws_user/README.md) to create the AWS user.

Once you have the credentials with you then run
>`$ ./setup config-credentials`
```
AWS is already configured.
validating the credentials ...
{
    "UserId": "AIXX7XXPXX4",
    "Account": "32XXXX",
    "Arn": "arn:aws:iam::XXXXX:user/terraform"
}
AWS Credentials configured successfully.
```

**Step 4)** *(optional)*
Now let's run `terraform plan` to understand the AWS resources that are going to get created.
>`$ ./setup terraform-plan`

### Before you run Step 4 commands:
> Note: If you have created a new AWS account then please make sure your account is verified and recieved a confirmation email from AWS.
  It may take 10-30 minutes after you setup your billing account with AWS and this is to ensure that we can spin up EC2 instances and other related resources.

**Step 5)**
Now let's run `terraform apply` to start creating the AWS resources that are required to setup `MediaWiki` deployment.
>`$ ./setup terraform-apply`

**Step 6)**
Once the EKS cluster is up and running we need to get the `kube conf` file to enable the `kubectl` command to communicate the cluster's REST API.
>`$ ./setup update-kubeconf`
```
Generating the kubeconfig file ...
!!Warning: you're about to overwrite the current kubectl config.
old config will be moved to /Users/$(whoami)/.kube/old_config.terraform_.
Do you still want to continue? (Y/n) y
copied the new config to ~/.kube/config
```

**Step 7)**
Now that we have the `kubectl` configured to communicate with our EKS cluster, let's add the `EC2` nodes that we have created to the `kubernetes` cluster as worker nodes.
>`$ ./setup init-k8s-nodes`

```
checking if worker nodes are initialised already ...
NAME                                            STATUS   ROLES    AGE   VERSION              INTERNAL-IP   EXTERNAL-IP      OS-IMAGE         KERNEL-VERSION                  CONTAINER-RUNTIME
ip-10-0-0-210.ap-southeast-1.compute.internal   Ready    <none>   65m   v1.13.7-eks-c57ff8   10.0.0.210    13.250.125.152   Amazon Linux 2   4.14.128-112.105.amzn2.x86_64   docker://18.6.1
ip-10-0-1-204.ap-southeast-1.compute.internal   Ready    <none>   65m   v1.13.7-eks-c57ff8   10.0.1.204    18.139.114.220   Amazon Linux 2   4.14.128-112.105.amzn2.x86_64   docker://18.6.1
```
**Step 8)**
The last step is to deploy the `MediaWiki` containers, create an external NLB, configure the `MediaWiki` server and generate the `LocalSettings.php` and store it as `configMap` and update the `deployment`.
>`$ ./setup deploy-app <env-name>`

```
creating deployments for mediawiki-staging ...
namespace "mediawiki-staging" deleted
cleaning up existing namespace ...
namespace/mediawiki-staging created
mediawiki-staging namespace was created.
deployment.apps/mediawiki-staging created
service/mediawiki-staging-nlb-service created
NAME                                 READY   STATUS              RESTARTS   AGE
mediawiki-staging-665f6f4b8d-zltf7   0/1     ContainerCreating   0          2s
Succesfully created the database and the user.
DBUser: wiki_user_staging, DBName: wiki_db_staging
PHP 7.2.20 is installed.
Found ImageMagick: /usr/bin/convert. Image thumbnailing will be enabled if you enable uploads.
Found the Git version control software: /usr/bin/git.
Using server URL "https://aaab39695a6de11e98d7206f2f3f1588-c2d8b3bb6b74e539.elb.ap-southeast-1.amazonaws.com".
Warning: Your default directory for uploads (/var/www/html/images/) is not checked for vulnerability to arbitrary script execution during the CLI install.
Using the PHP intl extension for Unicode normalization.
The environment has been checked. You can install MediaWiki.
Setting up database
done
Creating tables
done
Creating database user
done
Populating default interwiki table
done
Initializing statistics
done
Generating secret keys
done
Prevent running unneeded updates
done
Creating administrator user account
done
Creating main page with default content
done
Database was successfully set up
MediaWiki has been successfully installed. You can now visit <https://aaab39695a6de11e98d7206f2f3f1588-c2d8b3bb6b74e539.elb.ap-southeast-1.amazonaws.com> to view your wiki. If you have questions, check out our frequently asked questions list: <https://www.mediawiki.org/wiki/Special:MyLanguage/Manual:FAQ> or use one of the support forums linked on that page.
configmap/media-wiki-staging-config created
deployment.apps/mediawiki-staging configured
!! Please wait for 2-5 minutes for the DNS servers to propogate the address.
Access the mediawiki service at the URL: https://aaab39695a6de11e98d7206f2f3f1588-c2d8b3bb6b74e539.elb.ap-southeast-1.amazonaws.com
```

**Step 8)**
Now, if you'd like to destroy all the resources and instances then run the below command.
>`$ ./setup destroy`

### TODO:
- [x] Terraform init/AWS Credentials
- [x] IAM roles
- [x] Create VPC, subnets and security groups
- [x] Create EKS cluster with worker nodes (EC2 instances)
- [x] MySQL RDS instance (exposed within the VPC)
- [x] MediaWiki kubernetes deployment
- [ ] Persisted volumes to manage assets
- [x] Expose the service via NLB (k8s service)
- [x] Custom Dockerfile for MediaWiki
- [x] Handle multiple enviroments
	- [x] Create different databases
	- [x] Create k8s deployments and k8s services
	- [x] Manage storing the env specific credentials

