# Dockerfile

# Using mediawiki base image
# https://github.com/wikimedia/mediawiki-docker/blob/98c36e3a5bb64dca5cbe6638a826a7137a164a44/1.33/Dockerfile
FROM mediawiki:1.33.0
MAINTAINER Pradeep <itsprdp@gmail.com>

# Install common packages
RUN apt-get -qq update \
    && apt-get -qq install default-mysql-client -y \
    && apt-get -qq clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD ./scripts /var/www/scripts
