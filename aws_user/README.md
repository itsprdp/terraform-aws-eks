# Steps to create AWS user and assign roles

Create an IAM user with programmatic access and attach the "AdministratorAccess" policy and copy the `AccessKey` and `AccessSecret`.
Navigate to the [IAM Users](https://console.aws.amazon.com/iam/home?#/users) page in the AWS console.

### Step 1:
> Provide the `User name` you'd want to create and select the `Access Type` as `Programmatic Access` and click next.

![Step 1](./step_1.png)

### Step 2:
> Now, we need to set the user permissions. Let's do that by selecting the `Attach existing polices directly` tab and search for `AdministratorAccess` policy and check it and click next.

![Step 2](./step_2.png)

### Step 3:
> Let's leave the default settings as it is and click next here.

![Step 3](./step_3.png)

### Step 4:
> This is where you need to review the details provided in the previous steps and click "Create User".

![Step 4](./step_4.png)

### Step 5:
> Please note down the AWS `Access Key ID` and `Secret access key` either by downloading the CSV or by copying the text values displayed on the page. These values will be required for our script to configure AWS console API access to the terraform configurations to manage the cloud infrastructure.

![Step 5](./step_5.png)


Once, you're done with all the above steps head back to [Step 4](../README.md#installation-steps) to follow the next instructions.
