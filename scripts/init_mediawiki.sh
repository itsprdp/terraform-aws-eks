#!/usr/bin/env bash

SQL_HOST=$1
ROOT_USER=$2
ROOT_PASS=$3
DB_USER=$4
DB_PASS=$5
DB_NAME=$6
WIKI_NAME=$7
WIKI_PASS=$8
WIKI_HOST=$9

MAIN_PATH="/var/www/scripts"
SQL_CNF_PATH="$MAIN_PATH/tmp/mysql.cnf"

mkdir -p ${MAIN_PATH}/tmp

# setup MySQL connection command
myconf_template=`cat ${MAIN_PATH}/mysql_credentials.cnf`
eval "echo \"${myconf_template}\" > ${SQL_CNF_PATH}"

SQL_CMD="mysql --defaults-extra-file=$SQL_CNF_PATH"

CMD=`cat <<-SQL
$SQL_CMD -e "
DROP USER IF EXISTS '${DB_USER}'@'%';
DROP DATABASE IF EXISTS ${DB_NAME};
CREATE DATABASE ${DB_NAME};
CREATE USER '${DB_USER}'@'%' IDENTIFIED BY '${DB_PASS}';
GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USER}'@'%' IDENTIFIED BY '${DB_PASS}';"
SQL
`
eval $CMD

# check if we created the DB and User successfully
user=$(eval "$SQL_CMD -e \"SELECT User FROM mysql.user;\" | grep \"${DB_USER}\"")
name=$(eval "$SQL_CMD -e \"SHOW DATABASES;\" | grep \"${DB_NAME}\"")
echo "Succesfully created the database and the user."
echo "DBUser: $user, DBName: $name"

rm -rf $SQL_CNF_PATH

php /var/www/html/maintenance/install.php \
  --dbname=$DB_NAME --dbserver=$SQL_HOST\
  --installdbuser=$DB_USER --installdbpass=$DB_PASS \
  --dbuser=$DB_USER --dbpass=$DB_PASS\
  --server=http://${WIKI_HOST}\
  --scriptpath= --lang=en --pass=$WIKI_PASS "$WIKI_NAME" 'Admin'
