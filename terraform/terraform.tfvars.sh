#!/usr/bin/env bash

cat << EOF > ./terraform/terraform.tfvars
user-ip-address   = "$1"
database-user     = "$2"
database-password = "$3"
EOF
