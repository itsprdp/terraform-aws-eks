resource "aws_security_group" "crudapps-cluster" {
  name        = "terraform-eks-crudapps-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${aws_vpc.crudapps.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-eks-crudapps"
  }
}

resource "aws_security_group_rule" "crudapps-cluster-ingress-workstation-https" {
  cidr_blocks       = ["${var.user-ip-address}/32"]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.crudapps-cluster.id}"
  to_port           = 443
  type              = "ingress"
}
