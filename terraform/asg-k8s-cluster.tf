data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-${aws_eks_cluster.crudapps.version}-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

data "aws_region" "current" {}

# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  crudapps-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.crudapps.endpoint}' --b64-cluster-ca '${aws_eks_cluster.crudapps.certificate_authority.0.data}' '${var.cluster-name}'
USERDATA
}

resource "aws_launch_configuration" "crudapps" {
  associate_public_ip_address = true
  iam_instance_profile        = "${aws_iam_instance_profile.crudapps-node.name}"
  image_id                    = "${data.aws_ami.eks-worker.id}"
  instance_type               = "m4.large"
  name_prefix                 = "terraform-eks-crudapps"
  security_groups             = ["${aws_security_group.crudapps-node.id}"]
  user_data_base64            = "${base64encode(local.crudapps-node-userdata)}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "crudapps" {
  desired_capacity     = 2
  launch_configuration = "${aws_launch_configuration.crudapps.id}"
  max_size             = 2
  min_size             = 1
  name                 = "terraform-eks-crudapps"
  vpc_zone_identifier  = "${aws_subnet.crudapps.*.id}"

  tag {
    key                 = "Name"
    value               = "terraform-eks-crudapps"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster-name}"
    value               = "owned"
    propagate_at_launch = true
  }
}
