provider "aws" {
  region = "ap-southeast-1" # singapore
  shared_credentials_file = "../.aws/credentials"
  profile = "terraform"
}
