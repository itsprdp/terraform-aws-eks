data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "crudapps" {
  cidr_block = "10.0.0.0/16"

  tags = "${
    map(
     "Name", "terraform-eks-crudapps-node",
     "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"
}

resource "aws_subnet" "crudapps" {
  count = 2

  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block        = "10.0.${count.index}.0/24"
  vpc_id            = "${aws_vpc.crudapps.id}"

  tags = "${
    map(
     "Name", "terraform-eks-crudapps-node",
     "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"
}

resource "aws_internet_gateway" "crudapps" {
  vpc_id = "${aws_vpc.crudapps.id}"

  tags = {
    Name = "terraform-eks-crudapps"
  }
}

resource "aws_route_table" "crudapps" {
  vpc_id = "${aws_vpc.crudapps.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.crudapps.id}"
  }
}

resource "aws_route_table_association" "crudapps" {
  count = 2

  subnet_id      = "${aws_subnet.crudapps.*.id[count.index]}"
  route_table_id = "${aws_route_table.crudapps.id}"
}

// output "subnet_ids" {
//  value = "${aws_subnet.crudapps.*.id}"
// }
