resource "aws_eks_cluster" "crudapps" {
  name            = "${var.cluster-name}"
  role_arn        = "${aws_iam_role.crudapps-cluster.arn}"

  vpc_config {
    security_group_ids = ["${aws_security_group.crudapps-cluster.id}"]
    subnet_ids         = "${aws_subnet.crudapps.*.id}"
  }

  depends_on = [
    "aws_iam_role_policy_attachment.crudapps-cluster-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.crudapps-cluster-AmazonEKSServicePolicy",
  ]
}
