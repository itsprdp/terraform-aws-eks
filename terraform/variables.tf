variable "cluster-name" {
  default = "terraform-eks"
  type    = "string"
}

// Warning!
// Don't forget to pass the ENV variable when running terraform apply
// Eg: terraform apply -var user-ip-address=$(curl -s https://icanhazip.com)
variable "user-ip-address" {
  default = "0.0.0.0"
  type = "string"
}

variable "database-name" {
  default = "wikimedia_db"
  type = "string"
}

variable "database-user" {
  default = "wikimedia_db_user"
  type = "string"
}

variable "database-password" {
  default = "wikimedia_db_secret"
  type = "string"
}
