resource "aws_db_subnet_group" "crudapps-db" {
  name       = "crudapps-db"
  subnet_ids = "${aws_subnet.crudapps.*.id}"

  tags = "${
    map(
     "Name", "crudapps DB subnet group",
     "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"
}

resource "aws_db_instance" "crudapps-db" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  username             = "${var.database-user}"
  password             = "${var.database-password}"
  parameter_group_name = "default.mysql5.7"
  db_subnet_group_name = "${aws_db_subnet_group.crudapps-db.name}"

  skip_final_snapshot = true

  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  vpc_security_group_ids = [
    "${aws_security_group.crudapps-node.id}",
    "${aws_security_group.crudapps-cluster.id}",
  ]

  depends_on = [aws_db_subnet_group.crudapps-db]
}

output "rds_instance_ip" {
  value = "${aws_db_instance.crudapps-db.address}"
}
